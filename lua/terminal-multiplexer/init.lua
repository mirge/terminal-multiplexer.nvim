local M = {}

local defaults = require("terminal-multiplexer.defaults")

function M.setup(opts)
  M._config = vim.tbl_deep_extend("force", defaults, opts or {})

  require("terminal-multiplexer.keymaps").setup()
end

return M
