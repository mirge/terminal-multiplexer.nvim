return {
  prefix = "<C-s>",
  detach_keymap_func = function()
    require("terminal-multiplexer.utils").keymap.set("d", function()
      if vim.g.neovide then
        vim.fn.chanclose(vim.g.neovide_channel_id)
      else
        for _, ui in pairs(vim.api.nvim_list_uis()) do
          if ui.chan and not ui.stdout_tty then
            vim.fn.chanclose(ui.chan)
          end
        end
      end
    end, { noremap = true, desc = "Detach" }) -- see `utils.lua`
  end,
}
