local M = {}

local utils = require("terminal-multiplexer.utils")
local config = require("terminal-multiplexer")._config

if config == nil then
  return
end

function M.setup()
  config.detach_keymap_func()

  utils.keymap.set("%", function()
    vim.cmd.vsplit()
    vim.cmd.term()
  end, { desc = "Split vertically" })
  utils.keymap.set("\"", function()
    vim.cmd.split()
    vim.cmd.term()
  end, { desc = "Split horizontally" })

  utils.keymap.set("c", function()
    vim.cmd.tabnew()
    vim.cmd.term()
  end, { desc = "Create a new tab" })

  utils.keymap.set("n", vim.cmd.tabn, { desc = "Go to the next tab" })
  utils.keymap.set("p", vim.cmd.tabp, { desc = "Go to the previous tab" })

  vim.keymap.set("t", config.prefix .. "<Esc>", "<C-\\><C-n>", { desc = "Go back to Normal mode" })
end

return M
