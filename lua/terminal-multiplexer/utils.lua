local M = { keymap = {} }

local config = require("terminal-multiplexer")._config

if config == nil then
  return
end

function M.keymap.set(lhs, rhs, opts)
  vim.keymap.set({ "n", "t" }, config.prefix .. lhs, rhs, opts)
end

return M
